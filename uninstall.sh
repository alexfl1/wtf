#!/bin/bash

DIRNAME='/usr/bin/dirname'
READLINK='/bin/readlink'

CURRENT_DIR=$($DIRNAME $($READLINK -e "$0")) # Папка с деинсталятором

if [ -f $CURRENT_DIR'/setup.cfg' ]; then  # Проверка наличия файла конфигурации установки
. $CURRENT_DIR'/setup.cfg'		  # Подключение файла конфигурации 
else
echo "File $CURRENT_DIR'/setup.cfg' is not found"
exit 1
fi


#USER=$($ID -u)

if [ $($ID -u) == 0 ]; then   # Проверка рутовых прав
	echo "Privilege is good."
else
	echo "Please use \"sudo\" for this script"
	echo "Resualt: Uninstall proccess is broken"
	exit 2
fi

##### Составление списков на удаление
#
#       REMOVE_FILES
#       REMOVE_FOLDERS
#       UNFIND_FILES
#       UNFIND_FOLDERS

#ConfFiles=("$LINK", "$SETTINGS_GLOBAL", "$SETTINGS_HOME")
#ConfFolders=("$DST_PATH")

for F in ${ConfFiles[*]}; do
	if [[ -f $F ]]; then  # Проверка существования ссылки на программу
		REMOVE_FILES[${#REMOVE_FILES[*]}]=$F
	elif [[ -L $F ]]; then
		REMOVE_FILES[${#REMOVE_FILES[*]}]=$F
	else
		UNFIND_FILES[${#UNFIND_FILES[*]}]=$F
	fi
done

for D in ${ConfFolders[*]}; do
	if [ -d $D ]; then     # Проверка существования папки назначения
		REMOVE_FOLDERS[${#REMOVE_FOLDERS[*]}]=$D
	else
		UNFIND_FOLDERS[${#UNFIND_FOLDERS[*]}]=$D
	fi
done

echo "Файлы для удаления:"
	for i in ${REMOVE_FILES[*]}; do echo $i; done
echo "Папки для удаления:"
	for i in ${REMOVE_FOLDERS[*]}; do echo $i; done
echo "Не найденые файла:"
	for i in ${UNFIND_FILES[*]}; do echo $i; done
echo "Не найденые папки:"
	for i in ${UNFIND_FOLDERS[*]}; do echo $i; done


if [ "${#REMOVE_FILES[*]}" -eq 0 -a "${#REMOVE_FOLDERS[*]}" -eq 0 ]; then echo "Delete is nothing"; exit 0; fi


Question=''

while ! [ "$Question" = 'yes' -o "$Question" = 'no' ]; do
	#echo "1:Question=$Question"
	echo -n "Do need to delete founded files? - (yes/no): "
	read Question
	#echo "2:Question=$Question"
done

if [[ $Question == 'no' ]]; then echo "Uninstalling is abort"; exit 0; fi

echo "Uninstalling begin ..."

for i in ${REMOVE_FILES[*]}; do rm $i; done
for i in ${REMOVE_FOLDERS[*]}; do rm -rf $i; done

echo "Resualt: Uninstallation is complete"
