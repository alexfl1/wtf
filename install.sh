#!/bin/bash

DIRNAME='/usr/bin/dirname'
READLINK='/bin/readlink'

CURRENT_DIR=$($DIRNAME $($READLINK -e "$0")) # Путь папки с инсталятором

PATHS_CFG=$CURRENT_DIR'/setup.cfg' # Путь файла конфигуркации установки

if [ -f $PATHS_CFG ]; then  # Проверка существокания файла конфигурации
. $PATHS_CFG
else
   echo "File $PATHS_CFG is not found"
   exit 1
fi

#USER=$($ID -u)

if [ $($ID -u) == 0 ]; then # Проверка на права рута
	echo "Installing..."
else
	echo "Please use \"sudo\" for this script"
	echo "Resualt: Install proccess is broken"
	exit 2
fi


if ! [ -d $DST_PATH ]; then # Проверка существования папки установки
	mkdir $DST_PATH     # Создание папки при отсутсвие
	echo "Created destination path"
else
	echo "ERROR: Destination path is exist already"
	echo "Resualt: Install proccess is broken"
	exit 3
fi


cp -p  $CURRENT_DIR'/'* $DST_PATH # Копирование всех файлов в папку назначения
if [ $? ]; then
	echo "Files is copyed"
	chown -R root:root $DST_PATH # назначения хозяина и группы
else
	echo "Files is not copyed"
	echo "Resualt: Install proccess is broken"
	exit 4
fi

if [ -f $PROG ]; then	
	chmod a+x $PROG   # Исправление прав
	if [ $? ]; then
		echo "Permission is fixed"
	else
		echo "Error: Permission is NOT fixed"
	fi
fi

if ! [ -f $LINK ]; then # Проверка существования ссылки на программу
	ln -s  $PROG $LINK  # Создание ссылки при ее отсутсвие
	if [ $? ]; then
		echo "Link is created"
	else
		echo  "ERROR: Link is not created"
		echo "Resualt: Install proccess is broken"
		exit 5
	fi
else 
	echo "Link is exist"
fi

Question=''


while ! [ "$Question" = 'yes' -o "$Question" = 'no' ]; do
	echo -n "Do need to create /home/<user>/<config file> ? - (yes/no): "
	read Question
done


if [ $Question == 'yes' ]; then
	cp -p $SETTINGS_LOCAL  $SETTINGS_HOME 	
	if [ $? ]; then
		echo "User config file is created"
	else
		echo "ERROR: User config file is NOT created"
	fi 
fi

ln -s $SETTINGS_LOCAL $SETTINGS_GLOBAL
        if [ $? ]; then
                echo "Global config file is created"
        else
                echo "ERROR: Global config file is NOT created"
        fi



echo "Resualt: The installation is end."
